import pytest

from gepir import GEPIR


@pytest.fixture
def gepir(request):
    return GEPIR(requester_gln='0000000000000')
