from __future__ import absolute_import, division, print_function, unicode_literals

from future import standard_library

standard_library.install_aliases()

from builtins import *

from gepir import GEPIR
from gepir.elements import SOAPElement
from gepir.errors import AuthorizationFailed, DailyRequestLimitExceeded

NoneType = type(None)  # type: type


def soap_element_test(se):
    # type: (SOAPElement) -> None
    assert isinstance(se, SOAPElement)
    print(se)
    for e, p_t in se.elements_properties.items():
        p, t = p_t
        v = getattr(se, p)
        if isinstance(t, (tuple, list)):
            t = t[0] if t else None
            if t is not None:
                vs = v
                for v in vs:
                    assert(isinstance(v, t))
                    if isinstance(v, SOAPElement):
                        soap_element_test(v)
        else:
            try:
                assert isinstance(v, (t, NoneType))
            except TypeError as ee:
                print(e + ' ' + p + ' ' + repr(t))
                raise ee
            if isinstance(v, SOAPElement):
                soap_element_test(v)


def test_get_key_licensee(gepir):
    # type: (GEPIR) -> None
    for gtin in ('00037447250897',):
        try:
            gklr = gepir.get_key_licensee('GTIN', gtin)
            soap_element_test(gklr)
            assert gklr is not None
        except DailyRequestLimitExceeded:
            pass
    for gln in ('0037447121067',):
        try:
            gklr = gepir.get_key_licensee('GLN', gln)
            soap_element_test(gklr)
            assert gklr is not None
        except DailyRequestLimitExceeded:
            pass


def test_get_item_by_gtin(gepir):
    for gtin in (
        '04760000100013',
    ):
        try:
            gibgr = gepir.get_item_by_gtin(gtin)
            soap_element_test(gibgr)
            assert gibgr is not None
        except DailyRequestLimitExceeded:
            pass


def test_get_party_by_name(gepir):
    for country, company, city in (
        ('US', 'Leatherman', 'Portland'),
    ):
        try:
            gpbnr = gepir.get_party_by_name(
                company,
                requested_country=country,
                requested_city=city
            )
            soap_element_test(gpbnr)
            assert gpbnr is not None
        except DailyRequestLimitExceeded:
            pass


def test_get_prefix_licensee(gepir):
    for gtin in ('00037447250897',):
        try:
            gplr = gepir.get_prefix_licensee(
                'GTIN',
                gtin
            )
            soap_element_test(gplr)
            assert gplr is not None
        except DailyRequestLimitExceeded:
            pass


def test_get_root_directory(gepir):
    try:
        grdr = gepir.get_root_directory()
        soap_element_test(grdr)
        assert grdr is not None
    except AuthorizationFailed:
        pass


def test_get_router_detail(gepir):
    try:
        grdr = gepir.get_router_detail()
        soap_element_test(grdr)
        assert grdr is not None
    except AuthorizationFailed:
        pass


if __name__ == '__main__':
    test_get_item_by_gtin(GEPIR())